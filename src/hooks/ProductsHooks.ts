import React, { useState } from 'react'
import { productResponse } from '../models/productResponse.model';
import { getProductsAsync } from '../services/product.service';
import { useAsync } from './asyncComponentClean';
import useResponseAndLoad from './useResponseAndLoad';
let params = {
    filter: "",
    dataPage: {
      cantPage: 5,
      numberPage: 0,
    }
  }

const ProductsHooks = () => {
    const { loading, callEndpoint } = useResponseAndLoad();
	const [productsDataState, setProducts] = useState({} as productResponse);
    
    const dataProducts = (data: any) => {
        setProducts(data);
    };
    function searchProductsFilter (searchFilter : string) {
        params.filter = searchFilter;
        params.dataPage.numberPage = 0;
        const getApiDataR = async () => await callEndpoint(getProductsAsync(params));
        let isActive = true;
        const result = getApiDataR().then((response) => {
            if(isActive) {
                setProducts(response.data);
                isActive = false;
            }
        });
    };

    function searchProductsPaginate (numberPage : number) {
        params.dataPage.numberPage = numberPage - 1;
        const getApiDataR = async () => await callEndpoint(getProductsAsync(params));
        let isActive = true;
        const result = getApiDataR().then((response) => {
            if(isActive) {
                setProducts(response.data);
                isActive = false;
            }
        });
    };

    const getApiData = async () => await callEndpoint(getProductsAsync(params));
    useAsync(getApiData, dataProducts, () => {});

    return {
        productsDataState,
        searchProductsFilter,
        searchProductsPaginate
    };
}

export default ProductsHooks;