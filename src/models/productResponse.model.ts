export interface productResponse {
    success: number;
    maxPage: number;
    menssage: string;
    listData: Products[];
  }
  
  export interface Products {
    id: number;
    name: string;
    price: number;
  }
  