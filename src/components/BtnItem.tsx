import React, { useContext } from 'react'
import AppContext from '../context/AppContext';

const BtnItem = ({ position } : any) => {
    const { searchProductsPaginate } : any = useContext(AppContext);

    function eventPaginate() {
        searchProductsPaginate(position);
    }
    return (
        <button className='round' onClick={eventPaginate}>{position}</button>
    );
}

export default BtnItem;