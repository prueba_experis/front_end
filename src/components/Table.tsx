import React from 'react';
import '../assets/styles/table.scss'

const Table = ({products} : any ) => {
    return (
        <table>
            <thead>
                <tr>
                    <th>NOMBRE</th>
                    <th>PRECIO</th>
                </tr>
            </thead>
            <tbody>
                {products ?
                products.map((data: any) => (
                    <tr key={data.id}>
                        <td>{data.name}</td>
                        <td>{data.price}</td>
                    </tr>
                )) : <tr/>}
               
            </tbody>
        </table>
    );
}

export default Table;