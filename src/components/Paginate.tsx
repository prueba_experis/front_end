import React from 'react';
import BtnItem from './BtnItem';
import "../assets/styles/paginate.scss";
const Paginate = ({maxPage}: any) => {
    const position = [];
    for (let i = 1; i <= maxPage; i++) {
        position.push(i)
    }
    return (
        <div className='parent'>
            {position.map(pos => (
                <BtnItem position={pos} key={pos}></BtnItem>
            ))}
            
        </div>
    );
}

export default Paginate;