import React, { useContext } from 'react';
import "../assets/styles/input.scss"
import AppContext from '../context/AppContext';

const Search = () => {
    const { searchProductsFilter } : any = useContext(AppContext);

    function handleChange(event :any) {
        console.log(event.target.value);
        searchProductsFilter(event.target.value);
    }

    return (
        <input type={'search'} placeholder="buscar" className='searchInput' onChange={handleChange}></input>
    );
}

export default Search;