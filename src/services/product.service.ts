import { productResponse } from '../models/productResponse.model';
import { loadAbort } from '../utilities/abort-axios.utility';
import axios from 'axios';
import { API } from '../environments/environment';
const getProductsUrl = API+"GetProducts";

export const getProductsAsync = (params: any) => {
  const controller = loadAbort();
  return {
    call: axios.post<productResponse>(getProductsUrl,  params ),
    controller
  };
};

