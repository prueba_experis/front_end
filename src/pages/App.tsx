import { useContext, useState } from 'react';
import Paginate from '../components/Paginate';
import Search from '../components/Search';
import Table from '../components/Table';
import { useAsync } from '../hooks/asyncComponentClean';
import useResponseAndLoad from '../hooks/useResponseAndLoad';
import { productResponse } from '../models/productResponse.model';
import { getProductsAsync } from '../services/product.service';
import '../assets/styles/App.scss';
import AppContext from '../context/AppContext';
import ProductsHooks from '../hooks/ProductsHooks';

function App() {
  const initialState = ProductsHooks();

  return (
    <AppContext.Provider value={initialState}>
      <div className="App">
        <h1>Lista de Productos</h1>
            <Search></Search>
            <Table products={initialState.productsDataState.listData}></Table>
            <Paginate maxPage={initialState.productsDataState.maxPage}></Paginate>
      </div>
    </AppContext.Provider>
   
  );
}

export default App;
